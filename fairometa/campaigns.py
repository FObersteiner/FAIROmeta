# -*- coding: utf-8 -*-
"""Provide campaing-specific meta data"""
from pathlib import Path

import toml

try:
    wd = Path(__file__).parent
except NameError:
    wd = Path.cwd()  # same as os.getcwd()
assert wd.is_dir(), "failed to obtain working directory"

cfgfiles = (wd / "./campaigns/").glob("*.toml")
configs = {"_".join(f.stem.split("_")[2:]): f.resolve().as_posix() for f in cfgfiles}


def load(campaign_name: str) -> dict:
    """Load a campaign configuration."""

    if campaign_name not in (have := "\n".join(configs.keys())):
        raise ValueError(f"not found: {campaign_name}. Available:\n{have}")

    with open(configs[campaign_name], "r", encoding="UTF-8") as fp:
        meta_cfg = toml.load(fp)

    return meta_cfg
