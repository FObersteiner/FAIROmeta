# -*- coding: utf-8 -*-
from datetime import datetime, timezone
from pathlib import Path
from time import monotonic as ticker

import pandas as pd
import polars as pl
import xarray as xr
from fairopro import __version__ as fairopro_version
from fairopro.cfg import CfgBuilder
from fairopro.corr_time import correct_instrumenttime, set_range_t_exp
from fairopro.data_io import (
    ALIAS_OUT_CL,
    ALIAS_OUT_UV,
    DT_FMT,
    dump_dfs,
    load_data,
    save_data,
)
from fairopro.plotter import call_4_plots
from fairopro.process_cl import Flags_CL, calculate_cl_o3
from fairopro.process_uv import Flags_UV, calculate_uv_o3
from pyfuppes.na1001 import FFI1001

from fairometa import __version__ as fairometa_version
from fairometa import campaigns

# ------------------------------------------------------------------------------

FLIGHTS = [
    "F01",
    "F02",
    "F04",
    "F05",
    "F06",
    "F07a",
    "F07b",
    "F08",
    "F09",
    "F10a",
    "F10b",
    "F11",
    "F12",
    "F13",
    "F14",
    "F15",
    "F16",
    "F17",
    "F18",
    "F19",
    "F20",
]

# FLIGHTS = ["F01"]

CAMPAIGN = "PHILEAS_final"

PLOTS_DISABLED = True

WRITE_NASAAMES = True

WRITE_NC, NC_FORMAT = True, "NETCDF4"
NC4_FORMAT_DATE = "2023-12-14"
NC4_FORMAT_VERS = "0.1.2"

fairometa_repo = "https://gitlab.kit.edu/FObersteiner/FAIROmeta"
fairometa_doi = "10.5281/zenodo.11104076"  # 0.1.8
fairopro_repo = "https://gitlab.kit.edu/FObersteiner/FAIROpro"
fairopro_doi = "10.5281/zenodo.11104018"  # 0.4.1

nvvars_cfg = {
    "O3": {
        "standard_name": "mole_fraction_of_ozone_in_air",
        "unit": "ppb",
        "standard_unit": "1e-9",
    },
    "O3_unc": {
        "standard_name": "standard_error_of_mole_fraction_of_ozone_in_air",
        "unit": "ppb",
        "standard_unit": "1e-9",
    },
}

# ------------------------------------------------------------------------------

meta_cfg = campaigns.load(CAMPAIGN)

fairoprocfg = {
    no: Path(meta_cfg["flights"][no]["fairopro_cfg"]) for no in meta_cfg["flights"]  # type: ignore
}

for flight, cfgfile in fairoprocfg.items():
    if flight not in FLIGHTS:
        print(f"!! {flight} not found in {FLIGHTS}, skipping...")
        continue

    print(f"++ running {cfgfile.name}...")
    t0 = ticker()

    cfg = (
        CfgBuilder()
        .verbose(False)
        .write_output(0)
        .plots_close(False)
        .force_load_from_raw(0)
        .write_raw_merge_csv(0)
        .write_params_uv(("O3_ppb",))
        .write_params_cl(("O3_ppb",))
        .write_df_dump(0)
        .time_format("mdns")
        .output_float_precision(3)
        .build()
    )

    # --- LOAD CFG file
    cfg.load_params(cfgfile)

    # --- LOAD data
    dat = load_data(cfg)

    # --- PROCESS UV
    dat, cfg = calculate_uv_o3(dat, cfg)

    # --- PROCESS CL
    if cfg.params.process_cl and cfg.params.derived.uv_nvd > 3:
        dat, cfg = calculate_cl_o3(dat, cfg)

    # --- SET EXPERIMENT TIME RANGE
    if cfg.params.t_range_exp:
        dat, cfg = set_range_t_exp(dat, cfg)

    # --- MAP TIME TO IWG1 or manual fit/offset
    if cfg.params.process_t_corr:
        dat, cfg = correct_instrumenttime(dat, cfg)

    # --- PLOTS
    if not PLOTS_DISABLED:
        call_4_plots(dat, cfg)

    # --- OUTPUT
    if cfg.write_output:
        save_dat = save_data(dat, cfg).resolve()
        print(f"+++++\n+ data in\n+ {save_dat.as_posix()}")
    if cfg.write_output:
        save_cfg = cfg.save_params().resolve()
        print(f"+++++\n+ cfg file saved as {save_cfg.name} in\n+ {save_cfg.parent.as_posix()}")
    if cfg.write_df_dump:
        dst = dump_dfs(dat, cfg, cfg.write_df_dump)
        print(f"+++++\n+ dataframes dumped to\n+ {dst.as_posix()}")

    print(f"+++++\n+ done after {ticker() - t0:.2f} seconds\n+++++")

    # CL: use downsampled data by default
    clkey = "cl" if "cl_downsam" not in dat else "cl_downsam"

    if WRITE_NC:
        flight_key = cfg.params.exp_name.split("_")[0]
        now_utc = datetime.now(timezone.utc)
        for detector in ("uv", "cl"):
            filename = (
                meta_cfg["flights"][flight][f"out_fname_{detector.lower()}"]
                + meta_cfg["flights"][flight]["version_suffix"]
                + meta_cfg["filename_ext_netcdf"]
            )

            ivar, dvars = None, {"O3": None, "O3_unc": None}
            if detector == "uv":
                m = (dat["uv"]["flag"] == Flags_UV.OK) & (
                    ~dat["uv"].select(pl.col("mr")).to_series().is_null()
                )

                ivar = (
                    dat["uv"]
                    .filter(m)
                    .select(pl.col("v25_time"))
                    .to_numpy()
                    .T[0]
                    .astype("datetime64[ns]")
                )

                dvars["O3"] = dat["uv"].filter(m).select(pl.col("mr")).to_numpy().T[0]
                dvars["O3_unc"] = dat["uv"].filter(m).select(pl.col("mr_unc_abs")).to_numpy().T[0]
            elif detector == "cl" and cfg.params.process_cl and cfg.params.derived.uv_nvd > 3:
                m = (dat[clkey]["flag"] == Flags_CL.OK) & (
                    ~dat[clkey].select(pl.col("cl_mr")).to_series().is_null()
                )
                ivar = (
                    dat[clkey]
                    .filter(m)
                    .select(pl.col("v25_time"))
                    .to_numpy()
                    .T[0]
                    .astype("datetime64[ns]")
                )
                dvars["O3"] = dat[clkey].filter(m).select(pl.col("cl_mr")).to_numpy().T[0]
                dvars["O3_unc"] = (
                    dat[clkey].filter(m).select(pl.col("cl_mr_unc_abs")).to_numpy().T[0]
                )
            else:
                raise ValueError(f"invalid detector {detector}")

            if ivar is None:
                print(f"skip detector '{detector}'")
                continue

            coords = {
                "time": pd.DatetimeIndex(ivar),
            }

            data = {k: (["time"], v) for k, v in dvars.items()}

            ds = xr.Dataset(
                data,
                coords=coords,
            )

            ds["time"].attrs["axis"] = "T"
            ds["time"].attrs["coverage_content_type"] = "coordinate"
            ds["time"].attrs["standard_name"] = "time"
            ds["time"].attrs["name"] = (
                meta_cfg["col_header"][f"{detector.upper()}"][detector.lower()]
                .split("\n")[-1]
                .split("\t")[0]
            )

            # general (global) attributes
            ds.attrs["title"] = (
                meta_cfg["flights"][flight][f"out_fname_{detector.lower()}"]
                + meta_cfg["flights"][flight]["version_suffix"]
            )
            ds.attrs["date_created"] = (
                pd.Timestamp.now(tz="UTC").isoformat(timespec="seconds").replace("+00:00", "Z")
            )
            ds.attrs["mission"] = meta_cfg["MNAME"]
            ds.attrs["data_description"] = meta_cfg[f"SNAME_{detector.upper()}"]
            ds.attrs["data_institute"] = meta_cfg["ORG"]
            ds.attrs["data_owners"] = meta_cfg["ONAME"]
            ds.attrs["format_date"] = NC4_FORMAT_DATE
            ds.attrs["format_version"] = (
                f"{NC4_FORMAT_VERS}, written with xarray {xr.__version__} in Python"
            )
            ds.attrs["history"] = meta_cfg["flights"][flight]["changelog"]
            ds.attrs["exceptions"] = meta_cfg["flights"][flight]["exceptions"]
            ds.attrs["conventions"] = "CF-1.10"
            ds.attrs["comments"] = "\n".join(
                [
                    f"Flight: {cfg.params.exp_name}",
                    f"Platform: {cfg.params.exp_platform}",
                    f"Instrument: {cfg.params.instrument}",
                    f"FileName: {filename}",
                    f"FAIROmeta_version: {fairometa_version}, {fairometa_repo}, DOI: {fairometa_doi}",
                    f"FAIROpro_version: {fairopro_version}, {fairopro_repo}, DOI: {fairopro_doi}",
                    f"FAIROpro_cfgfile: {cfgfile.name}",
                ]
            )

            for p in dvars:
                ds[p].attrs["name"] = p
                ds[p].attrs["standard_name"] = nvvars_cfg[p]["standard_name"]
                ds[p].attrs["unit"] = nvvars_cfg[p]["unit"]
                ds[p].attrs["standard_unit"] = nvvars_cfg[p]["standard_unit"]
                ds[p].attrs["long_name"] = (
                    meta_cfg[f"SNAME_{detector.upper()}"].split("|")[-1].strip()
                )

            n = ds.to_netcdf(Path(meta_cfg["dst"]) / filename, format=NC_FORMAT)
            if n == 0:
                raise ValueError("no data written")

            print(f"+++++\n+ wrote netCDF {filename}\n+++++")

    if WRITE_NASAAMES:
        flight_key = cfg.params.exp_name.split("_")[0]
        now_utc = datetime.now(timezone.utc)
        for detector in ("uv", "cl"):
            filename = (
                meta_cfg["flights"][flight][f"out_fname_{detector.lower()}"]
                + meta_cfg["flights"][flight]["version_suffix"]
                + meta_cfg["filename_ext_nasaames"]
            )

            na = FFI1001()
            na.ONAME = meta_cfg["ONAME"]  # type: ignore
            na.ORG = meta_cfg["ORG"]  # type: ignore
            na.SNAME = meta_cfg[f"SNAME_{detector.upper()}"]  # type: ignore
            na.MNAME = meta_cfg["MNAME"]  # type: ignore
            na.DATE = cfg.params.exp_date  # type: ignore
            na.RDATE = [now_utc.year, now_utc.month, now_utc.day]  # type: ignore
            na.XNAME = meta_cfg[f"XNAME_{detector.upper()}"]  # type: ignore
            na.VNAME = meta_cfg["VNAME"]  # type: ignore

            na.VSCAL = meta_cfg["VSCAL"]  # type: ignore
            na.VMISS = [cfg.params[detector].vmiss] * len(na.VNAME)  # type: ignore

            na.SCOM = [
                f"Flight: {cfg.params.exp_name}",
                f"Platform: {cfg.params.exp_platform}",
                f"Instrument: {cfg.params.instrument}",
                f"FileName: {filename}",
                f"FAIROmeta_version: {fairometa_version}, {fairometa_repo}, DOI: {fairometa_doi}",
                f"FAIROpro_version: {fairopro_version}, {fairopro_repo}, DOI: {fairopro_doi}",
                f"FAIROpro_cfgfile: {cfgfile.name}",
                f"{meta_cfg['flights'][flight]['exceptions']}",
                f"{meta_cfg['flights'][flight]['changelog']}",
            ]

            na.NCOM = meta_cfg["NCOM"].split("\n") + meta_cfg["col_header"][f"{detector.upper()}"][
                detector.lower()
            ].split("\n")

            na.X = []
            na.V = [[], []]  # type: ignore

            io = na.to_file(
                Path(meta_cfg["dst"]) / filename,
                sep=" ",
                sep_data="\t",
                overwrite=True,
            )
            if not io:
                raise IOError(f"failed to write: {filename}")

            if detector == "uv":
                # keys to select from data
                keys_uv = ["mdns"] if cfg.time_format == "mdns" else ["v25_time"]
                keys_uv += [
                    v if (k := ALIAS_OUT_UV.get(v)) is None else k for v in cfg.write_params_uv
                ]
                # keys to rename
                if "v25_time" in keys_uv:
                    ALIAS_OUT_UV["datetime_UTC"] = "v25_time"
                # extract data and write
                with open(Path(meta_cfg["dst"]) / filename, "ab") as fp:
                    (
                        dat["uv"]
                        .filter(dat["uv"]["flag"] == Flags_UV.OK)
                        .select(
                            pl.col("mdns").map_elements(
                                lambda x: f"{x:{meta_cfg['format_time_uv']}}",
                                return_dtype=str,  # type: ignore
                            ),
                            pl.col("mr").map_elements(
                                lambda x: f"{x:{meta_cfg['format_o3_uv']}}",
                                return_dtype=str,  # type: ignore
                            ),
                            pl.col("mr_unc_abs").map_elements(
                                lambda x: f"{x:{meta_cfg['format_o3_uv_unc']}}",
                                return_dtype=str,  # type: ignore
                            ),
                        )
                        .write_csv(
                            fp,
                            include_header=False,
                            separator="\t",
                            datetime_format=DT_FMT,
                            float_precision=1,
                            null_value=str(cfg.params.uv.vmiss),
                        )
                    )
                print(f"+++++\n+ wrote NASA Ames {filename}\n+++++")

            if detector == "cl" and cfg.params.process_cl and cfg.params.derived.uv_nvd > 3:
                # keys to select from data
                keys_cl = ["mdns"] if cfg.time_format == "mdns" else ["v25_time"]
                keys_cl += [
                    v if (k := ALIAS_OUT_CL.get(v)) is None else k for v in cfg.write_params_cl
                ]
                # keys to rename
                if "v25_time" in keys_cl:
                    ALIAS_OUT_CL["datetime_UTC"] = "v25_time"
                # extract data and write
                with open(Path(meta_cfg["dst"]) / filename, "ab") as fp:
                    (
                        dat[clkey]
                        .filter(dat[clkey]["flag"] == Flags_CL.OK)
                        .select(
                            pl.col("mdns").map_elements(
                                lambda x: f"{x:{meta_cfg['format_time_cl']}}",
                                return_dtype=str,  # type: ignore
                            ),
                            pl.col("cl_mr").map_elements(
                                lambda x: f"{x:{meta_cfg['format_o3_cl']}}",
                                return_dtype=str,  # type: ignore
                            ),
                            pl.col("cl_mr_unc_abs").map_elements(
                                lambda x: f"{x:{meta_cfg['format_o3_cl_unc']}}",
                                return_dtype=str,  # type: ignore
                            ),
                        )
                        .write_csv(
                            fp,
                            include_header=False,
                            separator="\t",
                            datetime_format=DT_FMT,
                            float_precision=3,
                            null_value=str(cfg.params.cl.vmiss),
                        )
                    )
                print(f"+++++\n+ wrote NASA Ames {filename}\n+++++")
