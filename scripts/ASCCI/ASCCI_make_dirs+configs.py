"""Create folders for flights."""

from datetime import datetime, timezone

import toml
from fairopro import __version__ as fairoproversion
from fairopro.cfg import _call_taplo_fmt, master_cfg
from rich.pretty import pprint

from fairometa import __version__ as fairometaversion
from fairometa import campaigns, path

MAKE_BAHAMASinst = True


def dump_fairoprocfg(
    metadata: dict, experiment_name: str, faironame: str, make_link: bool = False
) -> None:
    expdata = metacfg["experiments"][experiment_name]
    cfg_fname = f"{expdata['expdate']}_{expdata['expname']}_FAIROpro.toml"
    tgt = f'{expdata["expdate"]}_{expdata["expname"]}__F{faironame}__'
    dst = path.clean(metacfg["data"]["cfg_dir"]) / cfg_fname
    pprint(f"make cfg file: {dst}")

    # - check if there is a FAIROpro config file
    if not dst.exists():
        defaults = toml.loads(master_cfg.decode("utf-8"))
        defaults.pop("state_coerce")
        defaults["exp_date"] = list(map(int, expdata["expdate"].split("-")))
        defaults["exp_name"] = expdata["expname"]
        defaults["exp_platform"] = metacfg["instrument"]["platform"]
        defaults["instrument"] = metacfg["instrument"]["instrument"]
        defaults["uv"]["f_led"] = metacfg["instrument"]["f_led"]
        defaults["uv"]["temperature_pos"] = "inside"
        defaults["path_input"] = (path.clean(metacfg["data"]["l0_dir"]) / tgt).as_posix()
        defaults["path_output"] = (path.clean(metacfg["data"]["l2_dir"]) / tgt).as_posix()

        header = f"""# FAIROpro config, created {datetime.now(timezone.utc).isoformat(timespec='seconds')}
# ***
# campaign: ASCCI
# ***
# FAIROmeta version: {fairometaversion}, FAIROpro version {fairoproversion}
# ***
# [group]
# KEY: Parameter
# ***
"""

        with open(dst, "w", encoding="UTF-8") as fp:
            fp.write(header)
        with open(dst, "a", encoding="UTF-8") as fp:
            toml.dump(defaults, fp)
        _call_taplo_fmt(dst, hide_output=True)

    else:
        pprint(f"skipped: {dst.name} exists")

    if make_link:
        linkdst = path.clean(metacfg["data"]["l2_dir"]) / tgt / cfg_fname
        pprint(f"make link: {linkdst}")
        try:
            linkdst.symlink_to(dst)
        except FileExistsError:
            pprint("skipped, file link exists.")


metacfg = campaigns.load("ASCCI_prelim")
srcs = (
    path.clean(metacfg["data"]["l0_dir"]),
    path.clean(metacfg["data"]["l2_dir"]),
)

fairos = ["1"]
tgts = []
l2_sub = ["doc"]
experiments = metacfg["experiments"]

have = {}
for fairoNo in fairos:
    pprint(f"~ FAIRO {fairoNo} dirs...")
    have[f"FAIRO-{fairoNo}"] = []
    for src in srcs:
        for experiment in experiments:
            tgt = f'{experiments[experiment]["expdate"]}_{experiments[experiment]["expname"]}__F{fairoNo}__'
            assert f"__F{fairoNo}__" in tgt
            have[f"FAIRO-{fairoNo}"].append(f"{(src / tgt).as_posix()}")
            try:
                (src / tgt).mkdir(mode=0o755, parents=True, exist_ok=False)
                pprint(f"created: {src / tgt}")
            except FileExistsError:
                pprint(f"skipped: {src / tgt}")

            if "L_2" in src.as_posix():
                dump_fairoprocfg(metacfg, experiment, fairoNo, True)
                for s in l2_sub:
                    try:
                        (src / tgt / s).mkdir(mode=0o755, parents=True, exist_ok=False)
                        pprint(f"created: {src / tgt / s}")
                    except FileExistsError:
                        pprint(f"skipped: {src / tgt / s}")


pprint("done, have directories:")
pprint(have)

if MAKE_BAHAMASinst:
    pprint("~ A/C data dirs... ~")
    dst = path.clean(metacfg["data"]["l2_dir"].replace("FAIRO-1", "BAHAMAS"))
    for experiment in experiments:
        tgt = f'{experiments[experiment]["expdate"]}_{experiments[experiment]["expname"]}'
        pprint(f"make {dst / tgt}")
        try:
            (dst / tgt).mkdir(mode=0o755, parents=True, exist_ok=False)
        except FileExistsError:
            pprint("skipped, directory exists.")
