# -*- coding: utf-8 -*-
from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr

FAIRO_DETECTORS = {"UV": "", "CL": "_CL"}


def plot_vs_Bahamas(
    file_bah: Path,
    file_o3: Path,
    normalize_y: bool = True,
    x_mdns: bool = True,
    ref_date="1970-01-01",
):
    ds_o3 = xr.load_dataset(file_o3)
    ds_bah = xr.load_dataset(file_bah)

    O3 = ds_o3["O3"]
    TAT = ds_bah["TAT"]
    H2O = ds_bah["MIXRATIOV"]

    labels = ["", "TAT_K", "H2O_mr"]
    labels[0] = "O3(CL)_ppb" if "FAIRO_CL_O3" in file_o3.name else "O3(UV)_ppb"

    if normalize_y:
        O3 = (ds_o3["O3"] - ds_o3["O3"].min()) / (ds_o3["O3"].max() - ds_o3["O3"].min())
        TAT = (ds_bah["TAT"] - ds_bah["TAT"].min()) / (
            ds_bah["TAT"].max() - ds_bah["TAT"].min()
        )  # noqa
        H2O = (ds_bah["MIXRATIOV"] - ds_bah["MIXRATIOV"].min()) / (
            ds_bah["MIXRATIOV"].max() - ds_bah["MIXRATIOV"].min()
        )
        labels = [l.split("_")[0] + "_norm" for l in labels]  # noqa

    x_O3 = ds_o3["O3"].time
    x_bah = ds_bah["TIME"]
    if x_mdns:
        x_O3 = (pd.to_datetime(x_O3) - pd.to_datetime(ref_date)).total_seconds()
        x_bah = (pd.to_datetime(x_bah.values) - pd.to_datetime(ref_date)).total_seconds()

    plt.plot(x_O3, O3, label=labels[0])
    plt.plot(x_bah, TAT, label=labels[1])
    plt.plot(x_bah, H2O, label=labels[2])
    plt.title(flight)
    plt.legend()
    plt.grid()
    plt.tight_layout()
    plt.show()


# -----------------------------------------------------------------------------

if __name__ == "__main__":

    flight = "CAFE-Pacific_27"
    fairo_use_detector = "CL"

    basepath = Path().home() / "Data/Campaigns/2023_12_CAFE_Pacific/INST/"
    assert basepath.exists()

    tmp = sorted(basepath.rglob(f"{flight}*_FAIRO{FAIRO_DETECTORS[fairo_use_detector]}_O3*.nc"))
    if not tmp:
        raise FileNotFoundError(f"no FAIRO data found for flight {flight} in {basepath.as_posix()}")

    # print(tmp)
    file_o3 = tmp[0]
    print(f"FAIRO : {file_o3.as_posix()}")

    ref_date = file_o3.name.split("_")[2]

    tmp = sorted(basepath.rglob(f"*{flight}*_BAHAMAS*.nc"))
    if not tmp:
        raise FileNotFoundError(f"no BAHAMAS data found for flight {flight}")
    file_bah = tmp[-1]
    print(f"BAHAMAS : {file_bah.as_posix()}")

    plot_vs_Bahamas(file_bah, file_o3, normalize_y=True, x_mdns=False, ref_date=ref_date)
