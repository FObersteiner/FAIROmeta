"""Create folders for flights."""

import os
import re
from pathlib import Path

import yaml

# where to save directory structure
dst = Path(
    "/home/va6504/Code/Python/FAIROmeta_extern/scripts/CAFE_Pacific/CAFE_Pacific_dirs_raw.yml"
)

srcs = [Path("/home/va6504/Data/Campaigns/2023_12_CAFE_Pacific/INST/FAIRO-1")]
for s in srcs:
    assert s.is_dir()

lvls = ["L_0", "L_2"]
l2_sub = ["doc"]

MAKE_BAHAMASinst = True
SAVE_DIRS = False

# targets must contain __F1__ or __F2__ for Fairo-1 / Fairo-2
TGTS = [
    "2023_12_14_RF01_Testflight__F1__",
    "2023_12_19_RF02_Testflight__F1__",
    "2024_01_09_RF03_ORYX__F1__",
    "2024_01_10_RF04_STINGRAY__F1__",
    "2024_01_11_RF05a_KANGAROO__F1__",
    "2024_01_12_RF05b_JOEY__F1__",
    "2024_01_16_RF06_WOMBAT__F1__",
    "2024_01_20_RF06x_Testflight__F1__",
    "2024_01_20_RF07_NEMO__F1__",
    "2024_01_23_RF08_KIRRILY__F1__",
    "2024_01_26_RF09_TAIPAN__F1__",
    "2024_01_27_RF10_MALOLO__F1__",
    "2024_01_31_RF11_KARUMBA__F1__",
    "2024_02_02_RF12_GALAH__F1__",
    "2024_02_04_RF13_PLATYPUS__F1__",
    "2024_02_07_RF14_CUSCUS__F1__",
    "2024_02_09_RF15a_TASMANIAN-DEVIL__F1__",
    "2024_02_09_RF15b_TASMANIAN-DEVIL__F1__",
    "2024_02_11_RF16_BANDICOOT__F1__",
    "2024_02_14_RF17_CASSOWARY__F1__",
    "2024_02_16_RF18_PETALURA__F1__",
    "2024_02_19_RF19_CROCODILE__F1__",
    "2024_02_22_RF20a_COCKATOO__F1__",
    "2024_02_22_RF20b_COCKATOO__F1__",
    "2024_02_23_RF21_QUOLL__F1__",
    "2024_02_24_RF22_LYREBIRD__F1__",
    "2024_02_28_RF23_LORIKEET__F1__",
    "2024_02_29_RF24_DINGO__F1__",
    "2024_03_02_RF26_LIONFISH__F1__",
    "2024_03_03_RF27_CAMEL__F1__",
]


have = {}
for fairoNo, src in enumerate(srcs):
    have[f"FAIRO-{fairoNo+1:d}"] = {}

    for level in lvls:
        have[f"FAIRO-{fairoNo+1:d}"][level] = []

        for title in TGTS:
            if f"__F{fairoNo+1:d}__" in title:
                print(f"\n~ FAIRO dirs... {title}~")
                try:
                    os.mkdir(src / level / title)
                    print("created:", src / level / title)
                except FileExistsError:
                    print("skipped:", src / level / title)
                    pass
                have[f"FAIRO-{fairoNo+1:d}"][level].append(f"{(src / level / title).as_posix()}")

                if level == "L_2":
                    for s in l2_sub:
                        try:
                            os.mkdir(src / lvls[1] / title / s)
                            print("created:", src / lvls[1] / title / s)
                        except FileExistsError:
                            print("skipped:", src / lvls[1] / title / s)
                            pass


if MAKE_BAHAMASinst:
    print("\n~ A/C data dirs... ~")
    srcs = {"lr": Path(str(srcs[0]).replace("FAIRO-1", "BAHAMAS"))}  # / lvls[1]}
    srcs["hr"] = srcs["lr"]
    for p in srcs.values():
        if not p.is_dir():
            p.mkdir(parents=True)

    have["BAHAMAS"] = {}
    for t in TGTS:
        if re.search("_[RF]F[0-9]{2}_", t) and ("__F1__" in t):
            t = t.replace("__F1__", "")
            flight = t.split("_")[-2]
            have["BAHAMAS"][flight] = {}
            for k, src in srcs.items():
                try:
                    os.mkdir(src / t)
                    print("created:", src / t)
                except FileExistsError:
                    print("skipped:", src / t)
                    pass
                have["BAHAMAS"][flight][k] = (src / t / "data").resolve().as_posix()


if SAVE_DIRS:
    assert dst.is_file()
    with open(dst, "w", encoding="UTF-8") as fobj:
        yaml.dump(have, fobj, default_flow_style=False, indent=4)
