## Concentratrion in ppb calculated using Lambert-Beer's law:
```
c = ln(I_0/I) / (sigma * l * n)
```
### at given p and T:
```
c[ppb] = ln(I_0/I) * 1 / (sigma_T * l * n) * (p_0/p) * ((T+T_0)/T_0) * 1e9
```
with
- `p_0`, `T_0`: 1013.25 hPa and 273.15 K reference pressure and temperature
- `I_O`: zero-ozone absorption
- `I`: absorption with sample air containing ozone
- `sigma_T`: ozone absorption cross-section in the Hartley band at 253.65 nm at given temperature
- `l`: absorption distance (length of both cuvettes)
- `n`: Loschmidt constant

### values
- `sigma_T = sigma_ref * (1 + T_corr)`
- `sigma_ref` [Barnes & Mauersberger 1987, DOI: 10.1029/JD092iD12p14861] `1.136e-21 m^2 / molecule`
- (currently unused !) `sigma_ref NEW` [Hodges et al., DOI: 10.1088/1681-7575/ab0bdd] `1.1329e-21 m^2 / molecule`

- `T_corr` [Barnes & Mauersberger 1987, DOI: 10.1029/JD092iD12p14861]
`3.09e-3 - 6.63e-5 * T - 3.88e-7 * T^2 - 3.18e-8 * T^3 - 4.01e-10 * T^4`

- `l`
`2 * 0.3784 m`

- Loschmidt constant (273.15 K, 101.325 kPa) [from https://physics.nist.gov/cgi-bin/cuu/Value?n0std] `2.686780111e25 m^-3`
