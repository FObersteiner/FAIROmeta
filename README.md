[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11275355.svg)](https://doi.org/10.5281/zenodo.11275355)

# FAIROmeta

FAIRO meta data, to describe output from [FAIROpro](https://gitlab.kit.edu/FObersteiner/FAIROpro). A data workflow schematic can be found [here](https://gitlab.kit.edu/FObersteiner/FAIROmeta/-/tree/main/technical/data_workflow).

## Content

```text
|
|-- /fairometa  # calibrations and campaign processing configurations
|
|-- /operation  # instrument operation docs; e.g. maintenance description
|
|-- /samples    # demo files with FAIRO data, in NASA Ames 1001, ICT and netCDF format
|
|-- /science    # instrument description and links to publications that use FAIRO data
|
|-- /scripts    # data processing scripts for specific campaigns
|
|-- /technical  # docs for various topics, such as data workflow, ozone mixing ratio calculation and instrument time synchronization
```

## FAIRO instrument

- Designed & built: Andreas Zahn (mostly)
- Operation & data processing: Florian Obersteiner (since 2018)
- [Description](https://gitlab.kit.edu/FObersteiner/FAIROmeta/-/blob/main/science/FAIRO_Description.md)

## Publications involving FAIRO ozone data

[FAIRO publications](https://gitlab.kit.edu/FObersteiner/FAIROmeta/-/blob/main/science/FAIRO_Publications.md)

## License

CC BY 4.0 - see LICENSE file in the root directory of the repository.
