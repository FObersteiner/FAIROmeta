# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 09:45:08 2022

@author: Florian Obersteiner, f.obersteiner@kit.edu

---

Temperature correction of the ozone absorption cross section.

We use the data from Barnes & Mauersberger 1987 (DOI: 10.1029/JD092iD12p14861) data,
to calculate the change in absorption cross section at 253.65 nm relative to
that at 297 K.

Since this is a relative change, we assume that it is independent of the
absolute value of the absorption cross section.
Therefore we assume that we can change that value  without invalidating
the temperature correction.

Barnes & Mauersberger 1987 used sigma = 1.136e-17 cm² / molecule at 297 K,
the value suggested by Hodges et al. 2019 (DOI: 10.1088/1681-7575/ab0bdd) is
1.1329e-17 cm²/ molecule (at 296 K, we neglect the difference to 297 K here).
"""

from io import StringIO

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# T in K, sigma in 1e-17 * cm^2 per molecule
data = """T,sigma
351,1.097
335,1.118
318,1.130
297,1.136
273,1.140
253,1.142
237,1.142
221,1.144
195,1.143
"""

df = pd.read_csv(StringIO(data))
df["T_degrC"] = df["T"] - 273.15

# relative diff to sigma at 297 K:
sigma_ref = df.loc[df["T"] == 297, "sigma"].values[0]
df["rel_diff"] = 100 * (df["sigma"] - sigma_ref) / sigma_ref

# calculate 4th order polynomial as fit function
parms = np.polyfit(df["T"].values, (df["rel_diff"]).values, 4)
print(parms[::-1])

df["rel_diff_fit"] = (
    parms[0] * df["T"] ** 4
    + parms[1] * df["T"] ** 3
    + parms[2] * df["T"] ** 2
    + parms[3] * df["T"]
    + parms[4]
)

# also include the fit suggested by Barnes & Mauersberger
bmparms = [3.09e-1, -6.63e-3, -3.88e-5, -3.18e-6, -4.01e-8][::-1]
df["rel_diff_BM"] = (
    bmparms[0] * df["T_degrC"] ** 4
    + bmparms[1] * df["T_degrC"] ** 3
    + bmparms[2] * df["T_degrC"] ** 2
    + bmparms[3] * df["T_degrC"]
    + bmparms[4]
)

# Fit used by Herman Smit (FZJ / WCCOS), personal communication 2022-08-29
sparms = [
    -153.7344183,
    +2.484907268,
    -0.01487415703,
    +0.00003931609929,
    -0.00000003883922904,
][::-1]
df["rel_diff_Smit"] = (
    sparms[0] * df["T"] ** 4
    + sparms[1] * df["T"] ** 3
    + sparms[2] * df["T"] ** 2
    + sparms[3] * df["T"]
    + sparms[4]
)


plt.plot(df["T"].values, df["rel_diff"], "b", marker="x", label="rel.diff to 297K")
plt.plot(df["T"].values, df["rel_diff_fit"], "r", linestyle="dashed", label="np polyfit")
plt.plot(df["T"].values, df["rel_diff_BM"], "g", linestyle="dashed", label="BM fit")
# plt.plot(df["T"].values, df["rel_diff_Smit"], "k", linestyle="dashed", label="Smit fit")
plt.title(
    f"y = {parms[-1]:.3e} + {parms[-2]:.3e}*T + {parms[-3]:.3e}*T^2\n"
    f"{parms[-4]:.3e}*T^3 + {parms[-5]:.3e}*T^4"
)
plt.xlabel("T[K]")
plt.ylabel("rel.diff to 297K [%]")
plt.legend()
plt.grid()
plt.show()
