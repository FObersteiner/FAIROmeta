# -*- coding: utf-8 -*-

import pathlib
import subprocess

try:
    wd = pathlib.Path(__file__).parent
except NameError:
    wd = pathlib.Path.cwd()  # same as os.getcwd()
assert wd.is_dir(), "faild to obtain working directory"


def call_taplo_fmt(fname: str | pathlib.Path, hide_output=False):
    """
    verify and format with taplo if available
    https://taplo.tamasfe.dev/
    """
    try:
        _ = subprocess.run(
            f"taplo fmt {fname} -- --test",
            shell=True,
            check=True,
            capture_output=hide_output,
        )
    except subprocess.CalledProcessError as e:
        print("missing taplo library, toml not re-formatted;")
        print(e)


print(wd.parent)
for p in sorted(wd.parent.rglob("*.toml")):
    print(f"format {p.as_posix()}")
    call_taplo_fmt(p)
