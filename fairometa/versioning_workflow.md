# Version Mgmt

## documents that specify the project (current) version

- ./CHANGELOG.md
- ./pyproject.toml
- ./CITATION.cff

## procedure for release

- update version in all aforementioned documents
- get a DOI for the next version on zenodo
- update/add DOI in CITATION.cff
- ? update DOI in campaign processing script
- verify that tests and linting run without errors
- push updates
- set the version as `git tag` and push the tag
- create a release on gitlab
