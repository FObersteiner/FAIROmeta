from pathlib import Path
from typing import Union


def clean(p: Union[Path, str], resolve: bool = True) -> Path:
    """Clean a path from stuff like ~ or $HOME."""
    path_str = Path(p).expanduser().as_posix()  # expanduser() resolves '~' to home path
    if path_str.startswith("$HOME"):
        path_str = path_str.replace("$HOME", Path().home().as_posix(), 1)

    if not resolve:
        return Path(path_str)
    return Path(path_str).resolve()
