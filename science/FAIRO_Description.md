# FAIRO

The **F**ast **Air**borne **O**zone instrument (FAIRO) is a fully custom-built, lightweight and compact ozone monitor. The target application is airborne measurements. Its unique measurement approach is a combination of

1. absolute quantification using a two-channel UV photometer that detects the absorption of O&#x2083; in the Hartley band around 254 nm
2. relative measurement using a fast chemiluminescence (CL) sensor with a sampling rate of 10 Hz and above [[Zahn et al. 2012](https://amt.copernicus.org/articles/5/363/2012/)].

The CL sensor detects photon emission from the reaction of O&#x2083; with a solid organic dye, coumarin [e.g. [Schurath et al. 1991](https://dx.doi.org/10.1007/BF00322426), [Güsten et al. 1992](https://dx.doi.org/10.1007/BF00115224), [Ermel et al. 2013](https://pubs.acs.org/doi/10.1021/es3040363)]. The CL response is continuously calibrated against the UV photometer data. This technique combines the accuracy of a UV photometer with the high measurement frequency of a CL detector.

## Typical measurement quality

- 1-sigma measurement precision: 0.3 ppb for the UV photometer measuring at 0.25 Hz and 0.5% for the CL detector measuring at 10 Hz
- UV photometer accuracy: 1.5% under optimal conditions, largely determined by the uncertainty of the O&#x2083; absorption cross section [e.g. [Barnes & Mauersberger 1987](https://dx.doi.org/10.1029/JD092iD12p14861), [Hodges et al. 2019](https://dx.doi.org/10.1088/1681-7575/ab0bdd)], plus the carry-over error from the calibration of the UV light source (LED)
- total uncertainty including calibration carry-over is typically 2% for the UV photometer and 2.5% for the CL detector (combined)
- conservative limits for field measurements: detection limit ~3 ppb, quantification limit ~9 ppb, upper detection limit > 5000 ppb

## Instrument dimensions

- weight: 14 kg
- width, height: 19’’, 3 height units

`last updated 2024-04-08`
