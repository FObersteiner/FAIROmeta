# example data

```text
|__ CL_detector # chemiluminescence detector, 12.5 Hz

|__ UV_photometer # UV photometer, reference, 0.25 Hz

```

processed data from UV photometer and CL detector;

- `.ames`: NASA Ames format, file format index 1001
- `.ict`: ICARTTv2 format, file format index 1001
- `.nc`: netCDF binary format

Source: HALO measurement campaign CIRRUS-HL and PHILEAS. The full data sets can be obtained via <https://halo-db.pa.op.dlr.de/>.
