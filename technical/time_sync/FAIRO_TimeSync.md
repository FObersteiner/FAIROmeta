# Time synchronization

To compare parameters measured by one instrument to those measured by another instrument or parameters from other sources, e.g. models, timestamps have to be synchronized. As reference for all sources, [UTC](https://en.wikipedia.org/wiki/Coordinated_Universal_Time) is used. For an in-situ instrument, "time synchronization" basically has two components:

**1 - Instrument and Platform.** The instrument's clock that is used to time-stamp the measurements has to be synchronized with the platform (e.g. the research aircraft), which provides the common time source for all instruments on the platform. The platform in turn needs to ensure that this time source is synchronized to UTC.

**2 - Actual sampling Time.** If in-situ measurements require sample medium to be transported to the detector, transport (and diffusion during that transport) time needs to be considered ("sample travel time"). For example, air from outside the aircraft needs to flow from the inlet to the instrument (detector). Sample travel time might be different for each instrument on one platform, which is why a correction of this time component is needed if measurements should be _directly_ comparable.

## Technical detail: FAIRO and data processing

- FAIRO has a clock built into its micro-controller, which can be set and adjusted manually to UTC. Based on the reading of the internal clock, each sample is time-stamped. But: Over time, the internal clock drifts relative to UTC, so measurement timestamps become more and more inaccurate.
- Although having some Ethernet capability, FAIRO does not have an [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol) client running, so its clock cannot be synced automatically to an NTP server.
- To account for the drifting clock, FAIRO records data received via Ethernet/UDP ([IWG1 string format](https://archive.eol.ucar.edu/raf/Software/iwgadts/IWG1_Def.html)), which also contains the aircraft clock timestamp.
- During data processing, FAIRO measurement timestamps are mapped to aircraft time by a linear fit (addressing **_1_**).
- As a post-processing step, measurement timestamps can be corrected for sample travel time from the air inlet to the instrument (addressing **_2_**). Volume flow through the inlet line is controlled and recorded by the instrument, and sampling line length and diameter are known (can be measured).
- As a second post-processing step, the accuracy of the time correction can be improved by [cross-correlation](https://en.wikipedia.org/wiki/Cross-correlation) analysis of the chemiluminescence ozone signal (up to 12.5 Hz) versus suitable parameters measured at comparable frequency by other instruments, e.g. [total air temperature (TAT)](https://en.wikipedia.org/wiki/Total_air_temperature) measurements from the aircraft's TAT probe.

### Sources of uncertainty

#### (pt. 1)

1. synchronization quality of platform to UTC
2. platform network latency (Ethernet is not a real-time system; UDP packets have a travel time)
3. data processing (parsing, recording etc.) on the instrument (10 MHz can't do magic...)
4. error of fit used to map instrument time to UTC

#### (pt. 2)

1. tubing diameter and length measurement
2. assumption of laminar flow
3. flow meter accuracy
4. cross correlation accuracy

### Typical values and: What is required?

The required time synchronization quality depends on the use case. One-second accuracy might be more than sufficient for comparison to a 3-hour model mean value. It might however be insufficient if the data should be used to analyze turbulence.

Some estimated values:

| source            | order   | direction | comment                                                     |
| :---------------- | :------ | :-------- | :---------------------------------------------------------- |
| 1.1/platform      | ?       | +/-       |                                                             |
| 1.2/network       | ~10 ms  | +         |                                                             |
| 1.3/dataproc(V25) | ~10 ms  | +         | potentially larger and/or variable depending on CPU load    |
| 1.4/fitting       | ~100 ms | +/-       |                                                             |
| 2.1/tubing        | ~100 ms | +/-       |                                                             |
| 2.2/flowmeter     | ?       | +/-       | also dependent on tubing length, could be attributed to 2.1 |
| 2.3/crosscorr     | ~ 10 ms | +/-       | very situation-dependent if method works at all             |

Note that these are _estimates_ based on experience during the last measurement campaigns. More thorough analysis is need for a meaningful error estimation (if required). Also, some error sources just add an offset (e.g. network latency; relatively constant), others might be more variable and/or go in both directions (negative or positive offset). If cross-correlation analysis (e.g. O3 vs. TAT) yields a clear maximum for a certain section of the time series, this can serve as a very good test for all the time corrections applied before.
