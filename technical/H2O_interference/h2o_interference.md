# Interference of FAIRO ozone measurements by water vapor

Water vapor (H&#x2082;O) content in the sample air can influence O&#x2083; measurements by FAIRO in multiple ways. The influence on the UV detector is critical. The chemiluminescence detector is also influenced, however it is only used as a relative measurement, calibrated by the UV photometer measurements.

## Indirect interference: ozone scrubber as a buffer

H&#x2082;O content of the air influences the transmission of UV light in FAIRO's photometer; here, less water vapor means better light transmission. FAIRO has a dual-channel UV photometer; the measurement procedure involves comparing the absorption of UV light by ozone on both channels (cuvettes). Ozone-free ("scrubbed") air is compared to sample air, in which you want to determine the ozone mixing ratio. For the comparison to be valid, H&#x2082;O content must be equal in both cuvettes. However, the ozone scrubber has the not-so-nice property of retaining water. Consequentially, after a sudden change in water vapor content of the sampled air, H&#x2082;O mixing ratio will **_not_** be equal in the two cuvettes, thus the determination of ozone content inaccurate. See also [Wilson & Birks 2006](https://doi.org/10.1021/es052590c). The scrubber is assumed to be the main buffer for moisture, as it has a very large surface compared to the tubing downstream and the cuvettes themselves. Any tubing etc. upstream of the scrubber affects both sample and zero air and is therefore irrelevant in this context.

TODO : add info on light source and collimation

### Effect on O&#x2083; quantification

- if H&#x2082;O _increases_ rapidly, the scrubber retains it, H&#x2082;O in the zero air cuvette is too low. More UV light reaches the detector. In terms of ozone that means the "zero" reference is lower than it actually should be. When referenced against the sample air cuvette (which has the increased H&#x2082;O), O&#x2083; will be **overestimated**
- if H&#x2082;O _decreases_ rapidly, the scrubber leaks "stored" H&#x2082;O into the zero air channel, H&#x2082;O in the zero air cuvette is too high. More H&#x2082;O = less light that reaches the detector. In terms of ozone the "zero" reference is higher than it actually should be, O&#x2083; will be **underestimated**

### Quantification: how much is too much?

- TODO : dH&#x2082;O in time and concentration domain

### Effect on chemiluminescence sensitivity and calibration

- an average concentration of O&#x2083; from the UV photometer is used for calibration. A median filter is applied for averaging with a time window of typically 110 seconds. The effect of short-term inaccuracies is therefore expected to be small (best case)
- the dry chemiluminescence detector sensitivity also has a significant positive H&#x2082;O dependence [[Schurath et al. 1991](https://dx.doi.org/10.1007/BF00322426), [Güsten et al. 1992](https://dx.doi.org/10.1007/BF00115224), [Zahn et al. 2012](https://dx.doi.org/doi:10.5194/amt-5-363-2012)], with a response time on the order of ~20s

## Indirect interference: ozone scrubber destruction

If liquid water reaches the ozone scrubber's catalyst (Hopkalit, Ger.: Braunstein), it can be destroyed, i.e. looses its ability to destroy ozone. If the scrubber does not remove 100% of ozone from the zero-reference channel, the determined O&#x2083; mixing ratio will be invalid. While this effect is rather disastrous, it can be avoided by preventing water from reaching the scrubber - in case of aircraft-based measurements for example by sampling through a backwards-facing inlet.

## Direct interference by absorption of UV light

- TODO : direct absorption of UV light within given wavelength boundaries by water vapor is negligible

## Effect on chemiluminescence detector

- coumarin emits photons when in contact with ozone; sensitivity is positively correlated with water vapor content. This is why it needs to be calibrated on a short timescale

## Potential remedies

- Nafion system:
  - the idea is to equilibrate H&#x2082;O between zero air (after passing the scrubber) and sample air, before the streams reach the cuvettes. If the interference from H&#x2082;O is the same for both cuvettes, the effect cancels out.
  - down-side: due to the additional flow path, response time will be decreased. This would be a less significant problem for FAIRO since it combines "slow-response" UV measurements with fast (~10 Hz) dry chemiluminescence measurements
- collimated light:
  - in theory, if the interference is caused by H2O interaction with the cuvettes, a collimated light source could avoid the problem: if the light path does not touch the cuvette walls, the only interference that can remain is that from the entrance lenses in front of the detector.
