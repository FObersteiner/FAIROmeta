from importlib import metadata

__version__ = metadata.version("fairometa")

from fairometa import (
    campaigns,
    path,
)

__version__ = metadata.version("fairometa")

__all__ = (
    "campaigns",
    "path",
)
