# -*- coding: utf-8 -*-
from datetime import datetime, timezone
from pathlib import Path

import toml
from fairopro import __version__ as fairoproversion
from fairopro.cfg import _call_taplo_fmt, master_cfg

from fairometa import __version__ as fairometaversion
from fairometa import campaigns

CAMPAIGN = "CAFE-Pacific_prelim"
metacfg = campaigns.load(CAMPAIGN)

for flight in metacfg["flights"]:
    print(flight)
    fdata = metacfg["flights"][flight]
    #
    dst = Path(fdata["fairopro_cfg"]).resolve()
    #
    # - check if there is a FAIROpro config file
    if dst.exists():
        print(f"skipped: {dst.name} exists")
        continue
    #
    # - if not, create a default one
    print(f"make file: {dst.name}")
    defaults = toml.loads(master_cfg.decode("utf-8"))

    defaults.pop("state_coerce")

    defaults["exp_date"] = list(map(int, fdata["flightdate"].split("-")))
    defaults["exp_name"] = fdata["flightname"]
    defaults["exp_platform"] = metacfg["instrument"]["platform"]
    defaults["instrument"] = metacfg["instrument"]["instrument"]

    defaults["uv"]["f_led"] = metacfg["instrument"]["f_led"]
    defaults["uv"]["temperature_pos"] = "inside"

    header = f"""# FAIROpro config, created {datetime.now(timezone.utc).isoformat(timespec='seconds')}
# ***
# campaign: {CAMPAIGN}
# ***
# FAIROmeta version: {fairometaversion}, FAIROpro version {fairoproversion}
# ***
# [group]
# KEY: Parameter
# ***
"""

    with open(dst, "w", encoding="UTF-8") as fp:
        fp.write(header)

    with open(dst, "a", encoding="UTF-8") as fp:
        toml.dump(defaults, fp)

    _call_taplo_fmt(dst, hide_output=True)

    lnk = dst.parent / dst.name.replace("toml", "link", 1)
    print(f"make link file: {lnk.name}")
    lnk.symlink_to(dst)
