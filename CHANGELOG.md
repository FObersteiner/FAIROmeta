# Changelog

<https://keepachangelog.com/>

Types of changes

- 'Added' for new features.
- 'Changed' for changes in existing functionality.
- 'Deprecated' for soon-to-be removed features.
- 'Removed' for now removed features.
- 'Fixed' for any bug fixes.
- 'Security' in case of vulnerabilities.

## [Unreleased]

### Changed

- updated data workflow

## 0.1.12, 2025-02-11

### Added

- prepare ASCCI config
- add processing scripts for ASCCI

### Changed

- move to uv for project management
- campaigns handling

## 0.1.11, 2024-07-16

- add CARIBIC-2 configs

## 0.1.10, 2024-06-04

- revise WCCOS side-by-side campaign configs

## 0.1.9, 2024-05-24

- CAFE-Pacific final
- add CARIBIC SOP

## 0.1.8, 2024-05-03

- add license, use CC BY 4.0 since this repo essentially contains data (configuration)
- update docs and examples

## 0.1.7, 2024-04-09

- PHILEAS final

## 0.1.6, 2024-03-19

- CAFE Pacific preliminary done

## 0.1.5, 2024-01-10

- new 'script' directory structure
- new scripts, starting with CAFE_Pacific: dirs_create and make_plots

## 0.1.4, 2023-12-13

- set tag for PHILEAS preliminary

## 0.1.3, 2023-08-28

- add WCCOS SbS 2 configs and output script

## 0.1.2, 2023-07-28

- add netCDF output
- revisions, PHILEAS

## 0.1.1, 2023-07-27

- begin revision of workflow for PHILEAS

## 0.1.0, 2023-07-18

- add some stuff for PHILEAS

## 0.1.0, 2023-02-10

- Repo created
- moved data here from FAIROpro, anything that is not code for data processing
