# -*- coding: utf-8 -*-
import pathlib
import unittest

from fairometa.campaigns import configs, load


class TestCfg(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.wd = pathlib.Path(__file__).parent
        except NameError:
            cls.wd = pathlib.Path.cwd()  # same as os.getcwd()
        assert cls.wd.is_dir(), "faild to obtain working directory"

    @classmethod
    def tearDownClass(cls):
        # to run after all tests
        pass

    def setUp(self):
        # to run before each test
        pass

    def tearDown(self):
        # to run after each test
        pass

    def test_campaigns(self):
        self.assertTrue(len(configs.keys()) > 5)

        valid = load("ASCCI_prelim")
        self.assertTrue(len(valid) > 0)

        with self.assertRaises(ValueError):
            load("nonexistent_campaign")


if __name__ == "__main__":
    unittest.main()
